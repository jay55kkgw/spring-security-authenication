package com.example.springsecurityauthenication.repository;

import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class UserDao {

    public Map<String,Map<String,String>> users;

    //模擬資料庫
    {

        //A01
        Map<String,String> info1 = new LinkedHashMap<>();
        info1.put("password","$2a$10$TPxnbhDwl3vR15wCmh57muIdwubTqv0xHkPWTFNR3peKQLM2Eu6Ua"); //1234
        info1.put("authority","admin,normal,ROLE_manager");

        //A02
        Map<String,String> info2 = new LinkedHashMap<>();
        info2.put("password","$2a$10$FMOhl2lNseiRKXwjVL1yeuWbmx2Wpy7n7Zbv6BqEXhtHa2fJHYBa6"); //5678
        info2.put("authority","normal,ROLE_employee");

        users = new LinkedHashMap<>();
        users.put("A01",info1);
        users.put("A02",info2);

        System.out.println(users);

    }

}
