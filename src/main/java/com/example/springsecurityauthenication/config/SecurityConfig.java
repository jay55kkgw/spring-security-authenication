package com.example.springsecurityauthenication.config;

import com.example.springsecurityauthenication.handle.MyAccessDenieHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

//若要自訂登入頁面則要繼承 WebSecurityConfiguration
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private MyAccessDenieHandler myAccessDenieHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //表單提交
        http.formLogin()
                //loginpage.html 表單 action 內容
                .loginProcessingUrl("/login")
                //自訂一登入頁面
                .loginPage("/loginPage")
                //登入成功之後頁面
                .successForwardUrl("/")
                //登入失敗之後頁面
                .failureForwardUrl("/fail");

        //授權認證
        http.authorizeHttpRequests()
                //不需要被認證頁面: /loginpage
                .antMatchers("/loginPage").permitAll()
                //權限判斷
                //必須要有 admin 權限才可訪問
                .antMatchers("/adminPage").hasAuthority("admin")
                //必須要有 manager 角色才可訪問
                .antMatchers("/managerPage").hasRole("manager")
                //其他指定任一角色都可以訪問
                .antMatchers("/employeePage").hasAnyRole("manager", "employee")
                //其他都要被認證
                .anyRequest().authenticated();

        //關閉 csrf 防護
        //http.csrf().disable();

        // 登出(不建議這樣寫 主要是練習)
        http.logout().deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/loginPage")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));//可以使用任何的 HTTP 方法登出

        //異常處理
        http.exceptionHandling()
                //.accessDeniedPage("/一長處理介面") // 請自行撰寫
                .accessDeniedHandler(myAccessDenieHandler);

        //勿忘我(remember-me)
        http.rememberMe()
                .userDetailsService(userDetailsService)
//                .tokenValiditySeconds(60 * 60 * 24 * 7);//一周
                .tokenValiditySeconds(60);//通常都會大於 session timeout 的時間
    }

    //注意!規定!要建立密碼演算的實例
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
