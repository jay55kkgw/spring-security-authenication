package com.example.springsecurityauthenication.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String welcome(){
        return "welcome";
    }

    @RequestMapping("/loginPage")
    public String loginPage(){
        return "loginPage";
    }

    @RequestMapping("/fail")
    @ResponseBody
    public String failPage(){
        return "fail";
    }

    @RequestMapping("/adminPage")
    @ResponseBody
    public String adminPage(){
        return "adminPage";
    }

    @RequestMapping("/managerPage")
    @ResponseBody
    public String managerPage(){
        return "managerPage";
    }

    @RequestMapping("/employeePage")
    @ResponseBody
    public String employeePage(){
        return "employeePage";
    }
}
