package com.example.springsecurityauthenication;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

class SpringSecurityAuthenicationApplicationTests {

    public static void main(String[] args) {

        PasswordEncoder pe = new BCryptPasswordEncoder();
        String encode = pe.encode("1234");
        System.out.println(encode);
        boolean matches = pe.matches("1234",encode);
        System.out.println(matches);

        System.out.println();

        String encode2 = pe.encode("5678");
        System.out.println(encode2);
        boolean matches2 = pe.matches("5678",encode2);
        System.out.println(matches2);

//        boolean matches3 = pe.matches("1234","$2a$10$TPxnbhDwl3vR15wCmh57muIdwubTqv0xHkPWTFNR3peKQLM2Eu6Ua");
//        System.out.println(matches3);
    }


}
